from django.urls import path, include
from .views import TripTransferList, PackageTransferView, AddCarrierToPackageTransfer, PopularTransfers
urlpatterns = [
    path('trips', TripTransferList.as_view(), name='trip_list'),
    path('package-transfers', PackageTransferView.as_view(), name='package-transfers'),
    path('package-transfers/<int:pk>/add-carrier', AddCarrierToPackageTransfer.as_view(), name='add-carrier'),
    path('populars/', PopularTransfers.as_view(), name='populars')
]

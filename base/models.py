from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.core.mail import send_mail
from django.utils.translation import gettext as _


from geoposition.fields import GeopositionField

# Create your models here.


def upload_avatar(instance, filename):
    return 'avatars/{}/{}'.format(instance.id, filename)

def upload_pacakge_photo(instance, filename):
    return 'packages/{}'.format(filename)

class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=True)
    avatar = models.ImageField(upload_to=upload_avatar, null=True, blank=True)
    is_staff = models.BooleanField(
        'staff status',
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    phone = models.CharField(max_length=33,null=True, blank=True)

    vk = models.URLField(null=True, blank=True)
    facebook = models.URLField(null=True, blank=True)
    instagram = models.URLField(null=True, blank=True)

    level = models.PositiveSmallIntegerField(default=1)
    experience = models.PositiveSmallIntegerField(default=100)

    is_in_blacklist = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)


class Review(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='reviews')
    text = models.TextField()
    score = models.FloatField()


class Carrier(User):

    passport = models.CharField(max_length=255, default='')
    passport_image = models.ImageField(null=True, blank=True)

    car = models.CharField(max_length=255, default='')
    car_number  = models.CharField(max_length=255, default='')


class Transfer(models.Model):
    from_point = GeopositionField()
    to_point = GeopositionField()

    time = models.DateTimeField()


class Package(models.Model):

    weight = models.PositiveSmallIntegerField(default=0)
    height = models.PositiveSmallIntegerField(default=0)
    depth = models.PositiveSmallIntegerField(default=0)

    photo = models.ImageField(upload_to=upload_pacakge_photo, null=True, blank=True)

    description = models.TextField()

    is_food = models.BooleanField(default=False)
    is_documents = models.BooleanField(default=False)


class PackageTransfer(Transfer):

    package = models.OneToOneField(Package, related_name='package_transfer', on_delete=models.CASCADE, null=True, blank=True)

    carriers = models.ManyToManyField(User, null=True, blank=True)
    is_urgently = models.BooleanField(default=False)
    auto_choose = models.BooleanField(default=False)

    is_closed = models.BooleanField(default=False)

    carrier= models.ForeignKey(Carrier, on_delete=models.SET_NULL, null=True, blank=True, related_name='packages')


class TripTransfer(Transfer):

    price = models.PositiveSmallIntegerField(default=0)

from django.shortcuts import render
from django.db.models import Avg, Count, Min, Sum
from .serializers import *
from .models import TripTransfer, Review, PackageTransfer, Carrier, Transfer
from django.shortcuts import get_object_or_404

from rest_framework.generics import ListCreateAPIView, CreateAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.

class TripTransferList(ListCreateAPIView):
    serializer_class = TripTransferSerializer
    queryset = TripTransfer.objects.all()


class PackageTransferView(ListCreateAPIView):
    serializer_class = PackageTransferSerializer
    queryset = PackageTransfer.objects.all()


class ReviewView(ListCreateAPIView):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()


class AddCarrierToPackageTransfer(CreateAPIView):
    queryset = PackageTransfer.objects.all()
    serializer_class = PackageTransferSerializer

    def post(self, request, *args, **kwargs):
        package_transfer = self.get_object()
        carrier = Carrier.objects.filter(user_ptr=request.user).first()

        if package_transfer.is_urgently:
            pass
        # If autochoose is selected assign carrier to package transfer, else add to carriers list
        if package_transfer.auto_choose:
            package_transfer.carrier = carrier

        else:
            package_transfer.carriers.add(carrier)
        package_transfer.save()
        return Response(self.get_serializer(package_transfer).data)


class PopularTransfers(APIView):
    def get(self, request, format=None):
        transfers = Transfer.objects.values('from_point', 'to_point').annotate(num_tic=Count('id'))
        serializer = PopularSerializer(transfers, many=True)
        return  Response(serializer.data)
from .models import Package, TripTransfer, PackageTransfer, Review

from rest_framework import serializers

class PackageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Package
        fields = '__all__'


class TripTransferSerializer(serializers.ModelSerializer):
    class Meta:
        model = TripTransfer
        extra = 1
        fields = '__all__'


class PackageTransferSerializer(serializers.ModelSerializer):
    package = PackageSerializer()
    class Meta:
        model = PackageTransfer
        fields  = '__all__'

    def create(self, validated_data):
        package_data= validated_data.pop('package')
        package = PackageSerializer.create(PackageSerializer(), package_data)
        package_transfer = super(PackageTransferSerializer, self).create(validated_data)
        package_transfer.package = package
        package_transfer.save()
        return package_transfer


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = '__all__'


class PopularSerializer(serializers.Serializer):
    from_point = serializers.DictField(child=serializers.DecimalField(max_digits=19, decimal_places=10), source='from_point.__dict__')
    to_point = serializers.DictField(child=serializers.DecimalField(max_digits=19, decimal_places=10), source='to_point.__dict__')

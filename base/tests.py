from django.test import TestCase, TransactionTestCase
from .models import PackageTransfer, Package
from model_mommy import mommy
# Create your tests here.
class CreatePackageTransferCase(TransactionTestCase):
    def setUp(self):
        self.package = mommy.make(Package)
        self.package_transfer = mommy.make(PackageTransfer, package=self.package,
                                           from_point="56.023,59.854",
                                           to_point="59.854, 56.023")

    def test_package_transfer(self):
        assert self.package_transfer

